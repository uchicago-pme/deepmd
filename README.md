# DeePMD on Midway 

Information on how to use and how to build the [DeepMD kit package](https://github.com/deepmodeling/deepmd-kit)
on midway. 

## How to Use 

A module was created for DeePMD within the midway2 module system. This currently
means you can only run DeePMD on nodes that have the Scientific Linux 7 operating
system. This includes all nodes that are from the broadwell generation or newer.
The current module for deepmd on midway has the cpu version of tensorflow as 
the backend, which means the current implementation can not use GPUs. A GPU version
can also be built. 

To load and activate the DeePMD environment: 

``` 
    module load deepmd
    source activate deepmd
```
This will also load the lammps version 5Jun19 module which has C++ bindings for 
interfacing with deepmd. 

## Build for DeePMD

This is a non trivial build that has special build requirements. The build must
be carried out on the `build` partition because you will require external network
access for the build, which is only available on the `build` partition compute 
node and because the bazel build tool is exteremely resource intensive, which 
necessitates it is not built on the logins. See the [build instructions](build/README.md)
for further information on building from source. 

