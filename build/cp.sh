#!/bin/bash
echo $PATH
which python 
module li
#
CURR_DIR=$(pwd)
export tensorflow_root=$CURR_DIR/tensorflow
mkdir $tensorflow_root/lib
cp $tensorflow_root/bazel-bin/tensorflow/libtensorflow_cc.so $tensorflow_root/lib/
cp $tensorflow_root/bazel-bin/tensorflow/libtensorflow_framework.so $tensorflow_root/lib/
cp /tmp/proto/lib/libprotobuf.a $tensorflow_root/lib/
cp /tmp/nsync/lib64/libnsync.a $tensorflow_root/lib/

mkdir -p $tensorflow_root/include/tensorflow
cp -r $tensorflow_root/bazel-genfiles/* $tensorflow_root/include/
cp -r $tensorflow_root/tensorflow/cc $tensorflow_root/include/tensorflow
cp -r $tensorflow_root/tensorflow/core $tensorflow_root/include/tensorflow
cp -r $tensorflow_root/third_party $tensorflow_root/include
cp -r /tmp/proto/include/* $tensorflow_root/include
cp -r /tmp/eigen/include/eigen3/* $tensorflow_root/include
cp -r /tmp/nsync/include/*h $tensorflow_root/include

cd $tensorflow_root/include
find . -name "*.cc" -type f -delete
