#!/bin/bash
echo $PATH
which python 
module load cmake
module li
#
CURR_DIR=$(pwd)
cd $CURR_DIR/tensorflow/tensorflow/contrib/makefile/downloads/eigen
echo "BUILDING EIGEN"
mkdir /tmp/eigen
mkdir build_dir
cd build_dir
cmake -DCMAKE_INSTALL_PREFIX=/tmp/eigen/ ../
make install
