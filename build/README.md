
# DeePMD Build 

## Dependencies: 
In order to build tensorflow from source (both the python and C++ tf libs) the
bazel build system is required. To build bazel run the bazel-build script: 
```
sh -x bazel-build.sh
```

## Build the CPU version of TF python libs
Have a look at the tf-py.sh script and adjust it accordingly. It needs to be 
able to locate bazel in order to build tf. 
*Note*: You must build the tf python and C++ libs from a compute node because
bazel is resource greedy and will use all available resources for some steps of 
the build. You also need to be able to have access to the external network for
which it is required that you build tf from the `build` partition on midway as 
this partition has a single node with external network access: 
```
sinteractive --partition=build --exclusive --time=04:00:00
```

Then proceed with running the tf build script. 
```
sh -x tf-py.sh
```

## Build the TF C++ Bindings
Again see the tf-build script and adjust accordingly. You will need to further
apply a patch. See the patch information at the following link: 
https://github.com/bazelbuild/bazel/issues/5348
Then proceed with running the build script: 
```
sh -x tf-build.sh
```

## Build the TF library dependencies 
Several other libraries are needed that you can build by running the tf-dependency.sh 
script: 

```
sh -x tf-dependency.sh 
```

## Build deepmd library
Have a look and adjust the deepmd.sh script as necessary, then proceed with running
it. 

```
sh -x deepmd.sh
```


