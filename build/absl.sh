#!/bin/bash
echo $PATH
which python 
module load cmake
module li
#
CURR_DIR=$(pwd)
export tensorflow_root=$CURR_DIR/tensorflow
cd $CURR_DIR/tensorflow/tensorflow/contrib/makefile/downloads/absl
echo "BUILDING ABSL"
bazel build
mkdir -p $tensorflow_root/include/
rsync -avzh --include '*/' --include '*.h' --exclude '*' absl $tensorflow_root/include/
cd ../../../../..
echo "DONE BUILDING ABSL"
echo $(pwd)
