#!/bin/bash

# SET CURRENT DIR
 BASE_DIR=$(pwd)

# LOAD BUILD DEPENDENT MODULES
 module load gcc/6.2
 module load Anaconda3/2018.12
 source activate deepmd
 module load cmake/3.11.0 # for build only
 module load java  # this is for bazel 

#
# EXPORT SOME ENV VARS
 export tensorflow_root=$BASE_DIR/tensorflow
 export deepmd_source_dir=$BASE_DIR/deepmd-kit
 export bazel_ver=0.16.0
 export bazel_dir=$tensorflow_root/bazel-$bazel_ver

#
# SET TENSORFLOW VERSION
 export tensorflow_version=1.12

  git clone https://github.com/tensorflow/tensorflow tensorflow -b "r$tensorflow_version" --depth=1 
  cd tensorflow
#
# INSTALL bazel version 
  wget https://github.com/bazelbuild/bazel/releases/download/$bazel_ver/bazel-$bazel_ver-dist.zip
  mkdir bazel-$bazel_ver
  cd bazel-$bazel_ver
  unzip ../bazel-$bazel_ver-dist.zip
  ./compile.sh
# EOF
