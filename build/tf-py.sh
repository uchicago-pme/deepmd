#!/bin/bash

# SET CURRENT DIR
 BASE_DIR=$(pwd)

# LOAD BUILD DEPENDENT MODULES
 module load gcc/6.2
 module load Anaconda3/2018.12
 source activate deepmd
 module load cmake/3.11.0 # for build only
 module load java  # this is for bazel 

#
# EXPORT SOME ENV VARS
 export tensorflow_root=$BASE_DIR/tensorflow
# EXPORT xdrfile_root=$BASE_DIR/xdrfile  
 export deepmd_source_dir=$BASE_DIR/deepmd-kit
 export bazel_ver=0.16.0
 export bazel_dir=$tensorflow_root/bazel-$bazel_ver

#
# SET TENSORFLOW VERSION
 export tensorflow_version=1.12

 export PATH=$bazel_dir/output:$PATH
 cd $tensorflow_root
# echo "THE BAZEL BINARY IS LOCATED AT $bazel_dir/output" 
 cp $BASE_DIR/input $tensorflow_root
 ./configure < input 
#
# BUILD TF 
  echo "BUILDING TENSORFLOW PYTHON LIB"
  bazel build --config=opt --verbose_failures  --copt=-mavx --copt=-mavx2 --local_resources 40480,20.0,2.0  //tensorflow/tools/pip_package:build_pip_package
  ./bazel-bin/tensorflow/tools/pip_package/build_pip_package /tmp/tensorflow_pkg
  pip install /tmp/tensorflow_pkg/tensorflow-$tensorflow_version-cp36-cp36m-linux_x86_64.whl 
#
# EOF
