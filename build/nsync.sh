#!/bin/bash
echo $PATH
which python 
module load cmake
module li
#
CURR_DIR=$(pwd)
cd $CURR_DIR/tensorflow/tensorflow/contrib/makefile/downloads/nsync
echo "BUILDING NSYNC"
mkdir /tmp/nsync
mkdir build_dir
cd build_dir
cmake -DCMAKE_INSTALL_PREFIX=/tmp/nsync/ ../
make
make install
echo "DONE BUILDING NSYNC"
