#!/bin/bash
echo $PATH
which python 
module li
#
CURR_DIR=$(pwd)
cd $CURR_DIR/tensorflow 
echo "BUILDING PROTOBUF"
mkdir /tmp/proto
sed -i 's;PROTOBUF_URL=.*;PROTOBUF_URL=\"https://mirror.bazel.build/github.com/google/protobuf/archive/v3.6.0.tar.gz\";g' tensorflow/contrib/makefile/download_dependencies.sh
tensorflow/contrib/makefile/download_dependencies.sh
cd tensorflow/contrib/makefile/downloads/protobuf/
./autogen.sh
./configure --prefix=/tmp/proto/
make
make install
