#!/bin/bash
# 
# LOAD CMAKE AND GCC MODULES
module load cmake
module load gcc/6.2 
#module load Anaconda3/2018.12
#source activate deepmd
#
# GET THE DEEPMD REPO
  git clone https://github.com/deepmodeling/deepmd-kit.git deepmd-kit 
#
# SET THE CC AND CXX COMPILERS FOR CMAKE -- CMAKE WILL NOT DETECT THE CORRECT ONE SO YOU MUST DO THIS
export CC=/software/gcc-6.2-el7-x86_64/bin/gcc
export CXX=/software/gcc-6.2-el7-x86_64/bin/g++
#
# SET PATHS
CURR_DIR=$(pwd)
export deepmd_root=/software/deepmd-0.12.4-el7-x86_64
export tensorflow_root=$CURR_DIR/tensorflow
cd deepmd-kit
deepmd_source_dir=$(pwd)
cd $deepmd_source_dir/source
mkdir build 
cd build

#cmake -DTF_GOOGLE_BIN=true -DTENSORFLOW_ROOT=$tensorflow_root -DCMAKE_INSTALL_PREFIX=$deepmd_root ..
cmake  -DTENSORFLOW_ROOT=$tensorflow_root -DCMAKE_INSTALL_PREFIX=$deepmd_root ..
make -j20
make install
#
# check build is ok
echo "ALL DONE WITH DEEPMD BUILD"
ls $deepmd_root/bin
sleep 10s
echo "MAKE LAMMPS EXTENSION FOR DEEPMD"
cd $deepmd_source_dir/source/build
make lammps
echo "LAMMPS EXTENSION PATH IS:"
echo "$deepmd_source_dir/source/build"
