#!/bin/bash

# SET CURRENT DIR
 BASE_DIR=$(pwd)

# LOAD BUILD DEPENDENT MODULES
 module load gcc/6.2   # make sure the gcc version is as close to the version in the python distribution
                         # otherwise there are problems linking some of the shared C++ libs. 
# module load Anaconda3/2018.12
# source activate deepmd
 module load cmake/3.11.0 # for build only
 module load java  # this is for bazel 

#
# EXPORT SOME ENV VARS
 export tensorflow_root=$BASE_DIR/tensorflow
# EXPORT xdrfile_root=$BASE_DIR/xdrfile  
 export deepmd_source_dir=$BASE_DIR/deepmd-kit
 export bazel_dir=$tensorflow_root/bazel-0.16.0
#
# SET TENSORFLOW VERSION
 export tensorflow_version=1.12
# 
echo "BUILDING OTHER TF DEPENDENCIES" 
#
# BUILD PROTOBUF
sh -x ./protobuf.sh
#
# BUILD ABSL
sh -x ./absl.sh
#
# BUILD EIGEN
sh -x ./eigen.sh
#
# BUILD NSYNC
sh -x ./nsync.sh
#
# COPY LIBS AND HEADER FILES FROM TF DEPENDENCY BUILDS TO TENSORFLOW_ROOT FOLDER
sh -x ./cp.sh

#
# EOF 


